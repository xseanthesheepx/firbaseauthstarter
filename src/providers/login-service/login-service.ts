import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { HomePage } from '../../pages/home/home';
import { AlertController} from 'ionic-angular';

/*
  Generated class for the LoginServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class LoginService {
  user: Observable<firebase.User>;

  constructor(public afAuth: AngularFireAuth, private alertCtrl: AlertController) {}

    loginUser(newEmail: string, newPassword: string): Promise<any> {
      return this.afAuth.auth.signInWithEmailAndPassword(newEmail, newPassword);
    }

    resetPassword(email: string): Promise<any> {
      return this.afAuth.auth.sendPasswordResetEmail(email);
    }

    logoutUser(): Promise<any> {
      return this.afAuth.auth.signOut();
    }

    signupUser(newEmail: string, newPassword: string): Promise<any> {
      return this.afAuth.auth.createUserWithEmailAndPassword(newEmail, newPassword);
    }

    
  }
