import { Component } from '@angular/core';


import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { HomePage } from '../home/home';
import { ResetPage } from '../reset/reset';
import { LoginService } from '../../providers/login-service/login-service';
import {
  IonicPage,
  NavController,
  LoadingController,
  Loading,
  AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EmailValidator } from '../../providers/email-validator/email-validator';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  public loginForm:FormGroup;
  public loading:Loading;

  loggedIn: any;

  constructor(public navCtrl: NavController, public authData: LoginService,
    public formBuilder: FormBuilder, public alertCtrl: AlertController,
    public loadingCtrl: LoadingController) {

      this.loginForm = formBuilder.group({
         email: ['', Validators.compose([Validators.required, EmailValidator.isValid])],
         password: ['', Validators.compose([Validators.minLength(6), Validators.required])]
      });
    }

    loginUser(){
      if (!this.loginForm.valid){
        console.log(this.loginForm.value);
      } else {
        this.authData.loginUser(this.loginForm.value.email, this.loginForm.value.password)
        .then( authData => {
          localStorage.setItem('loginData', authData);
          this.navCtrl.setRoot(HomePage);
        }, error => {
          this.loading.dismiss().then( () => {
            let alert = this.alertCtrl.create({
              message: error.message,
              buttons: [
                {
                  text: "Ok",
                  role: 'cancel'
                }
              ]
            });
            alert.present();
          });
        });

        this.loading = this.loadingCtrl.create({
          dismissOnPageChange: true,
        });
        this.loading.present();
      }
  }

  googleLogin(){
    this.authData.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider())
    .then( authData => {
      localStorage.setItem('loginData', authData);
        this.navCtrl.setRoot(HomePage);
  },error => {
      let alert = this.alertCtrl.create({
        message: error.message,
        buttons: [
          {
            text: "Ok",
            role: 'cancel'
          }
        ]
      });
      alert.present();
    });
  }



  facebookLogin(){
    this.authData.afAuth.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider())
    .then( authData => {
      localStorage.setItem('loginData', authData);
        this.navCtrl.setRoot(HomePage);
  },error => {
      let alert = this.alertCtrl.create({
        message: error.message,
        buttons: [
          {
            text: "Ok",
            role: 'cancel'
          }
        ]
      });
      alert.present();
    });
  }
  

  goToResetPassword(){
    this.navCtrl.push('ResetPage');
  }

  createAccount(){
    this.navCtrl.push('SignupPage');
  }

}
